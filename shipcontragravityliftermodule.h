#ifndef SHIPCONTRAGRAVITYLIFTERMODULE_H
#define SHIPCONTRAGRAVITYLIFTERMODULE_H
#include "shipmodule.h"
#include "spaceshipclass.h"
#include "numberparser.h"

class ShipContragravityLifterModule : public ShipModule
{
public:
    ShipContragravityLifterModule(int hullPosition, int moduleSlot, SpaceshipClass *ship);
    ~ShipContragravityLifterModule();
    QString text() const;
    virtual void setItemVisibleText();

private:
    SpaceshipClass *ship;
};

#endif // SHIPCONTRAGRAVITYLIFTERMODULE_H

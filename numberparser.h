#ifndef NUMBERPARSER_H
#define NUMBERPARSER_H

#include <QString>

/*
 * A collection of various number text parsers that can
 * be called statically.
 */
class NumberParser
{
public:
    NumberParser();
    ~NumberParser();
    static QString addCommas(QString number);
    static QString formatHumanReadable(QString number);
    static qint64 getWorkspacesForSystem(int SM);
    static qint64 getCost1(int SM, int mod);
    static qint64 getCost2(int SM, int mod);
    static int getdDRValue(int SM);

private:
    std::vector<float> dDRLookupTable;
};

#endif // NUMBERPARSER_H

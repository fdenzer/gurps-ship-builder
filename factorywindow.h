#ifndef FACTORYWINDOW_H
#define FACTORYWINDOW_H

#include <QMainWindow>

namespace Ui {
class FactoryWindow;
}

class FactoryWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit FactoryWindow(QWidget *parent = 0);
    ~FactoryWindow();

private:
    Ui::FactoryWindow *ui;
};

#endif // FACTORYWINDOW_H

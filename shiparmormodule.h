#ifndef SHIPARMORMODULE_H
#define SHIPARMORMODULE_H
#include "shipmodule.h"
#include "numberparser.h"


class ShipArmorModule : public ShipModule
{
public:
    ShipArmorModule(int hullPosition, int moduleSlot);
    ~ShipArmorModule();
    int getdDR();
    void setdDR(int newdDR);
    QString text() const;
    QString getArmorType();
    void setArmorType(QString type);
    virtual void setItemVisibleText();

protected:

private:
    int dDR;
    QString armorType;
};

#endif // SHIPARMORMODULE_H

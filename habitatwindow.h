#ifndef HABITATWINDOW_H
#define HABITATWINDOW_H

#include <QMainWindow>

namespace Ui {
class HabitatWindow;
}

class HabitatWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit HabitatWindow(QWidget *parent = 0);
    ~HabitatWindow();

private:
    Ui::HabitatWindow *ui;
};

#endif // HABITATWINDOW_H

#ifndef SHIPROBOTARMMODULE_H
#define SHIPROBOTARMMODULE_H
#include "shipmodule.h"
#include "spaceshipclass.h"
#include "numberparser.h"

class ShipRobotArmModule : public ShipModule
{
public:
    ShipRobotArmModule(int hullPosition, int moduleSlot, SpaceshipClass *ship);
    ~ShipRobotArmModule();
    QString text() const;
    virtual void setItemVisibleText();

private:
    SpaceshipClass *ship;
};

#endif // SHIPROBOTARMMODULE_H

#include "factorywindow.h"
#include "ui_factorywindow.h"

FactoryWindow::FactoryWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::FactoryWindow)
{
    ui->setupUi(this);
}

FactoryWindow::~FactoryWindow()
{
    delete ui;
}

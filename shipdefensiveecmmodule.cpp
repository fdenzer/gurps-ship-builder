#include "shipdefensiveecmmodule.h"

ShipDefensiveECMModule::ShipDefensiveECMModule(int hullPosition, int moduleSlot, SpaceshipClass *ship)
{
    this->moduleSlot = moduleSlot;
    this->hullPosition = hullPosition;
    this->highPower = false;
    this->ship = ship;
    totalWorkspaces = NumberParser::getWorkspacesForSystem(ship->getClassSM());
    this->cost = NumberParser::getCost2(ship->getClassSM(), ship->getClassSM()+10);
}

ShipDefensiveECMModule::~ShipDefensiveECMModule()
{

}

QString ShipDefensiveECMModule::text() const
{
    QString returnValue = "";
    returnValue += "[" + QString::number(moduleSlot);
    if (highPower)
    {
        returnValue += "!]";
    }
    else
    {
    returnValue += "]";
    }
    returnValue += " Defensive ECM";
    returnValue += " Workspaces: " + QString::number(totalWorkspaces);
    returnValue += " Cost: " + NumberParser::formatHumanReadable(QString::number(cost));
    return returnValue;
}

/**
 * @brief ShipDefensiveECMModule::setItemVisibleText
 * Sets the visible text when the item is in,
 * e.g., a QListWidget.
 */
void ShipDefensiveECMModule::setItemVisibleText()
{
    setData(Qt::DisplayRole, this->text());
}

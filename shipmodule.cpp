#include "shipmodule.h"

/**
 * @brief ShipModule::ShipModule
 */
ShipModule::ShipModule()
{
    hullPosition = 0;
    moduleSlot = 0;
    highPower = false;
    totalWorkspaces = 0;
}

/**
 * @brief ShipModule::ShipModule
 * @param debugText
 * @param hullPosition
 * @param moduleSlot
 */
ShipModule::ShipModule(QString debugText, int hullPosition, int moduleSlot)
{
    this->debugText = debugText;
    this->moduleSlot = moduleSlot;
    this->hullPosition = hullPosition;
    setData(Qt::DisplayRole, debugText);
    ShipModule();
}

ShipModule::~ShipModule()
{

}

/**
 * @brief ShipModule::getHullPosition
 * @return
 */
int ShipModule::getHullPosition()
{
    return hullPosition;
}

/**
 * @brief ShipModule::getSlot
 * @return
 */
int ShipModule::getSlot()
{
    return moduleSlot;
}

/**
 * @brief ShipModule::isHighPower
 * @return
 */
bool ShipModule::isHighPower()
{
    return highPower;
}

int ShipModule::getWorkspacesNeeded()
{
    return totalWorkspaces;
}

/**
 * @brief ShipModule::getCost
 * @return
 */
int ShipModule::getCost()
{
    return cost;
}

/**
 * @brief ShipModule::setHullPosition
 * @param position
 */
void ShipModule::setHullPosition(int position)
{
    hullPosition = position;
}

/**
 * @brief ShipModule::setSlot
 * @param slot
 */
void ShipModule::setSlot(int slot)
{
    moduleSlot = slot;
}

/**
 * @brief ShipModule::setHighPower
 * @param highPowah
 */
void ShipModule::setHighPower(bool highPowah)
{
    highPower = highPowah;
}

/**
 * @brief ShipModule::setWorkspaces
 * @param workspaces
 */
void ShipModule::setWorkspaces(int workspaces)
{
    totalWorkspaces = workspaces;
}

/**
 * @brief ShipModule::setCost
 * @param price
 */
void ShipModule::setCost(int price)
{
    cost = price;
}

/**
 * @brief ShipModule::text
 * @return
 */
QString ShipModule::text() const
{
    QString returnValue = "";
    if (hullPosition == FRONT)
    {
        returnValue += "Front";
    }
    else if (hullPosition == CENTRAL)
    {
        returnValue += "Center";
    }
    else if (hullPosition == REAR)
    {
        returnValue += "Rear";
    }
    else if (hullPosition == CORE)
    {
        returnValue += "Core";
    }
    returnValue += " [" + QString::number(moduleSlot);
    if (highPower)
    {
        returnValue += "!]";
    }
    else
    {
    returnValue += "]";
    }
    returnValue += debugText;
    return returnValue;
}

/**
 * @brief ShipModule::setItemVisibleText
 */
void ShipModule::setItemVisibleText()
{
    setData(Qt::DisplayRole, text());
}

#ifndef SHIPOVERVIEW_H
#define SHIPOVERVIEW_H

#include <QMainWindow>
#include "spaceshipclass.h"
#include "armorwindow.h"
#include "numberparser.h"
#include "shipmodule.h"
#include "shipcloakingdevicemodule.h"
#include "shipdefensiveecmmodule.h"
#include "shipexternalclampmodule.h"
#include "shiprobotarmmodule.h"
#include "shipcontragravityliftermodule.h"

//class ArmorWindow;

namespace Ui {
class ShipOverview;
}

class ShipOverview : public QMainWindow
{
    Q_OBJECT

public:
    explicit ShipOverview(QWidget *parent = 0);
    ~ShipOverview();
    void addModule(ShipModule *module);

private slots:
    void on_smBox_valueChanged(int arg1);
    void on_classNameBox_editingFinished();
    void on_checkBox_stateChanged(int arg1);
    void exitWindow();
    void armorWindow();
    void addCloakingDevice();
    void addDefensiveECM();
    void addExternalClamp();
    void addRoboticArm();
    void addContragravityLifter();


private:
    Ui::ShipOverview *ui;
    SpaceshipClass *ship;
    int getSlotPositionInSection(int oldPosition);
};

#endif // SHIPOVERVIEW_H

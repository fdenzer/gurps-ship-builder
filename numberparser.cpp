#include "numberparser.h"

NumberParser::NumberParser()
{
}

NumberParser::~NumberParser()
{

}

/**
 * @brief NumberParser::addCommas
 * @param number
 * @return
 * Add an appropriate number of commas to any number
 * rendered as a QString first. E.g. 1000 -> 1,000.
 */
QString NumberParser::addCommas(QString number)
{
    QString returnString = number;
    int commas = (returnString.size()-1)/3;
    int position = returnString.size()-3;
    for (int i = 1; i <= commas; i++)
    {
        returnString.insert(position, ",");
        position -= 3;
    }
    return returnString;
}

/**
 * @brief NumberParser::formatHumanReadable
 * @param number
 * @return
 * Rounds numbers to the nearest thousand, million,
 * billion, or trillion as appropriate. It then appends
 * the appropriate prefix symbol.
 */
QString NumberParser::formatHumanReadable(QString number)
{
    QString returnString = number;
    int thousands = (returnString.size()-1)/3;
    qint64 temp = number.toLongLong();
    switch (thousands) {
    case 1:
        //10*((n+5)/10)
        temp = 1000*((temp+500)/1000);
        returnString.chop(3);
        returnString += "K";
        break;
    case 2:
        temp = 1000000*((temp+500000)/1000000);
        returnString.chop(6);
        returnString += "M";
        break;
    case 3:
        temp = 1000000000*((temp+500000000)/1000000000);
        returnString.chop(9);
        returnString += "B";
        break;
    case 4:
        temp = 1000000000000*((temp+500000000000)/1000000000000);
        returnString.chop(12);
        returnString += "T";
        break;
    }
    return returnString;
}

qint64 NumberParser::getWorkspacesForSystem(int SM)
{
    int workspace;
    if (SM < 9)
    {
        return 0;
    }
    int power = (SM-10)/2;
    if (SM == (SM/2)*2)
    {
        workspace = 1*((int)(pow(10,power)+0.5));
    }
    else
    {
        workspace = 3*((int)(pow(10,power)+0.5));
    }
    return workspace;
}

qint64 NumberParser::getCost1(int SM, int mod)
{
    qint64 cost;
    int power = (mod)/2;
    if (SM == (SM/2)*2)
    {
        cost = 3*((qint64)(pow(10,power)+0.5));
    }
    else
    {
        cost = 1*((qint64)(pow(10,power)+0.5));
    }
    return cost;
}

qint64 NumberParser::getCost2(int SM, int mod)
{
    qint64 cost;
    int power = (mod)/2;
    if (SM == (SM/2)*2)
    {
        cost = 1*((qint64)(pow(10,power)+0.5));
    }
    else
    {
        cost = 3*((qint64)(pow(10,power)+0.5));
    }
    return cost;
}

int NumberParser::getdDRValue(int SM)
{
    //basicTable[(SM+1)%6] * 10^((SM+1)/6))
    float lookup = SM%6;
    if (lookup == 0)
    {
        lookup = 1;
    }
    else if (lookup == 1)
    {
        lookup = 1.5;
    }
    else if (lookup == 2)
    {
        lookup = 2;
    }
    else if (lookup == 3)
    {
        lookup = 3;
    }
    else if (lookup == 4)
    {
        lookup = 5;
    }
    else if (lookup == 5)
    {
        lookup = 7;
    }

    int dDR = lookup*((int)(pow(10,SM/6)+0.5));
    return dDR;
}


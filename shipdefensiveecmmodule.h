#ifndef SHIPDEFENSIVEECMMODULE_H
#define SHIPDEFENSIVEECMMODULE_H
#include "shipmodule.h"
#include "spaceshipclass.h"
#include "numberparser.h"


class ShipDefensiveECMModule : public ShipModule
{
public:
    ShipDefensiveECMModule(int hullPosition, int moduleSlot, SpaceshipClass *ship);
    ~ShipDefensiveECMModule();
    QString text() const;
    virtual void setItemVisibleText();

private:
    SpaceshipClass *ship;
};

#endif // SHIPDEFENSIVEECMMODULE_H

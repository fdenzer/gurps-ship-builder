#include "armorwindow.h"
#include "ui_armorwindow.h"

/**
 * @brief ArmorWindow::ArmorWindow
 * @param parent The parent of this window
 * @param ship A valid pointer to a SpaceshipClass object
 * @param hullPosition Whether the module is in the Front, Center, Rear, or Core
 * @param slot Which sub-position within the hull this module is.
 */
ArmorWindow::ArmorWindow(ShipOverview *parent, SpaceshipClass *ship, int hullPosition, int slot) :
    QMainWindow(parent),
    ui(new Ui::ArmorWindow)
{
    ui->setupUi(this);
    shipWindowParent = parent;
    this->ship = ship;
    module = new ShipArmorModule(hullPosition, slot);

    //Add all possible armor types to the list.
    ui->armorList->addItem("Ice");
    ui->armorList->addItem("Stone");
    ui->armorList->addItem("Steel");
    ui->armorList->addItem("Light Alloy");
    ui->armorList->addItem("Metallic Laminate");
    ui->armorList->addItem("Advanced Metallic Laminate");
    ui->armorList->addItem("Nanocomposite");
    ui->armorList->addItem("Organic");
    ui->armorList->addItem("Diamondoid");
    ui->armorList->addItem("Exotic Laminate");

    //Set the unstreamlined dDR, streamlined dDR, and cost of each armor type.
    armorUdDR["Ice"] =                        { 0, 0, 0, 1, 2, 2, 3, 5, 7, 10, 15};
    armorSdDR["Ice"] =                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    armorCost["Ice"] =                        { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    armorUdDR["Stone"] =                      { 0, 0, 1, 2, 2, 3, 5, 7, 10, 15, 20};
    armorSdDR["Stone"] =                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    armorCost["Stone"] =                      { 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
    armorUdDR["Steel"] =                      { 1, 2, 3, 5, 7, 10, 15, 20, 30, 50, 70};
    armorSdDR["Steel"] =                      { 0, 1, 2, 3, 5, 7, 10, 15, 20, 30, 50};
    armorCost["Steel"] =                      { 6000, 20000, 60000, 200000, 600000, 2000000, 6000000, 20000000, 60000000, 200000000, 600000000};
    armorUdDR["Light Alloy"] =                { 2, 3, 5, 7, 10, 15, 20, 30, 50, 70, 100};
    armorSdDR["Light Alloy"] =                { 1, 2, 3, 5, 7, 10, 15, 20, 30, 50, 70};
    armorCost["Light Alloy"] =                { 15000, 50000, 150000, 500000, 1500000, 5000000, 15000000, 50000000, 150000000, 500000000, 1500000000};
    armorUdDR["Metallic Laminate"] =          { 3, 5, 7, 10, 15, 20, 30, 50, 70, 100, 150};
    armorSdDR["Metallic Laminate"] =          { 2, 3, 5, 7, 10, 15, 20, 30, 50, 70, 100};
    armorCost["Metallic Laminate"] =          { 30000, 100000, 300000, 1000000, 3000000, 10000000, 30000000, 100000000, 300000000, 1000000000, 3000000000};
    armorUdDR["Advanced Metallic Laminate"] = { 5, 7, 10, 15, 20, 30, 50, 70, 100, 150, 200};
    armorSdDR["Advanced Metallic Laminate"] = { 3, 5, 7, 10, 15, 20, 30, 50, 70, 100, 150};
    armorCost["Advanced Metallic Laminate"] = { 60000, 200000, 600000, 2000000, 6000000, 20000000, 60000000, 200000000, 600000000, 2000000000, 6000000000};
    armorUdDR["Nanocomposite"] =              { 7, 10, 15, 20, 30, 50, 70, 100, 150, 200, 300};
    armorSdDR["Nanocomposite"] =              { 5, 7, 10, 15, 20, 30, 50, 70, 100, 150, 200};
    armorCost["Nanocomposite"] =              { 150000, 500000, 1500000, 5000000, 15000000, 50000000, 150000000, 500000000, 1500000000, 5000000000, 15000000000};
    armorUdDR["Organic"] =                    { 2, 3, 5, 7, 10, 15, 20, 30, 50, 70, 100};
    armorSdDR["Organic"] =                    { 1, 2, 3, 5, 7, 10, 15, 20, 30, 50, 70};
    armorCost["Organic"] =                    { 10000, 30000, 100000, 300000, 1000000, 3000000, 10000000, 30000000, 100000000, 300000000, 1000000000};
    armorUdDR["Diamondoid"] =                 { 10, 15, 20, 30, 50, 70, 100, 150, 200, 300, 500};
    armorSdDR["Diamondoid"] =                 { 7, 10, 15, 20, 30, 50, 70, 100, 150, 200, 300};
    armorCost["Diamondoid"] =                 { 300000, 1000000, 3000000, 10000000, 30000000, 100000000, 300000000, 1000000000, 3000000000, 10000000000, 30000000000};
    armorUdDR["Exotic Laminate"] =            { 15, 20, 30, 50, 70, 100, 150, 200, 300, 500, 700};
    armorSdDR["Exotic Laminate"] =            { 10, 15, 20, 30, 50, 70, 100, 150, 200, 300, 500};
    armorCost["Exotic Laminate"] =            { 600000, 2000000, 6000000, 20000000, 60000000, 200000000, 600000000, 2000000000, 6000000000, 20000000000, 60000000000};
}

ArmorWindow::~ArmorWindow()
{
    delete module;
    delete ui;
}

/**
 * @brief ArmorWindow::on_armorList_currentTextChanged
 * @param currentText
 * When any item with a different text is selected, get
 * the related values and store them to module.
 */
void ArmorWindow::on_armorList_currentTextChanged(const QString &currentText)
{
    module->setArmorType(currentText);
    int shipSM = ship->getClassSM();
    module->setCost(armorCost[currentText][shipSM-5]);
    bool shipStreamlined = ship->getClassStreamlined();
    ui->smDisplay->setText(QString::number(shipSM));
    ui->costDisplay->setText(NumberParser::addCommas(QString::number(armorCost[currentText][shipSM-5])));
    if (shipStreamlined)
    {
        ui->streamlinedDisplay->setText("True");
        ui->dDRDisplay->setText(QString::number(armorSdDR[currentText][shipSM-5]));
        module->setdDR(armorSdDR[currentText][shipSM-5]);
    }
    else
    {
        ui->streamlinedDisplay->setText("False");
        ui->dDRDisplay->setText(QString::number(armorUdDR[currentText][shipSM-5]));
        module->setdDR(armorUdDR[currentText][shipSM-5]);
    }
    if (currentText == "Ice" && shipStreamlined)
    {
            ui->costDisplay->setText("Invalid");
            ui->dDRDisplay->setText("Invalid");
    }
    else if (currentText == "Stone" && shipStreamlined)
    {
            ui->costDisplay->setText("Invalid");
            ui->dDRDisplay->setText("Invalid");
    }
    else if (currentText == "Exotic Laminate")
    {
        ui->dDRDisplay->setText(QString::number(NumberParser::getdDRValue(shipSM+2)));
    }
}

/**
 * @brief ArmorWindow::on_cancelButton_clicked
 */
void ArmorWindow::on_cancelButton_clicked()
{
    this->hide();
    delete module;
    delete ui;
}
/**
 * @brief ArmorWindow::on_insertButton_clicked
 */
void ArmorWindow::on_insertButton_clicked()
{
    //parent->addM
    //(ShipOverview *) shipWindowParent->addModule(module);
    //module->text();
    shipWindowParent->addModule(module);
    this->hide();
    delete ui;
}

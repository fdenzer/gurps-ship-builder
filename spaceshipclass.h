#ifndef SPACESHIPCLASS_H
#define SPACESHIPCLASS_H

#include <QString>
#include <math.h>
#include "shipmodule.h"


class SpaceshipClass
{
public:
    SpaceshipClass();
    ~SpaceshipClass();
    QString getClassName();
    void setClassName(QString name);
    int getClassSM();
    void setClassSM(int sm);
    int getClassTonnage();
    bool getClassStreamlined();
    void setClassStreamlined(bool set);

private:
    QString className;
    int classSM;
    bool classStreamlined;
    std::vector<std::vector<ShipModule>> modules;
};

#endif // SPACESHIPCLASS_H

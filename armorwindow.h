#ifndef ARMORWINDOW_H
#define ARMORWINDOW_H

#include <QMainWindow>
#include "shipoverview.h"
#include "spaceshipclass.h"
#include <QHash>
#include "numberparser.h"
#include "shiparmormodule.h"

enum hullPositions { FRONT, CENTRAL, REAR, CORE };

namespace Ui {
class ArmorWindow;
}

//Forward declaration to resolve circular dependency issues.
class ShipOverview;

class ArmorWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit ArmorWindow(ShipOverview *parent, SpaceshipClass *ship, int hullPosition, int slot);
    ~ArmorWindow();

private slots:
    void on_armorList_currentTextChanged(const QString &currentText);
    void on_cancelButton_clicked();
    void on_insertButton_clicked();

private:
    Ui::ArmorWindow *ui;
    ShipOverview *shipWindowParent;
    SpaceshipClass *ship;
    ShipArmorModule *module;
    QHash<QString, std::vector<qint64>> armorCost;
    QHash<QString, std::vector<int>> armorUdDR;
    QHash<QString, std::vector<int>> armorSdDR;
};

#endif // ARMORWINDOW_H

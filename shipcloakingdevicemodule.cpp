#include "shipcloakingdevicemodule.h"

ShipCloakingDeviceModule::ShipCloakingDeviceModule(int hullPosition, int moduleSlot, SpaceshipClass *ship)
{
    this->moduleSlot = moduleSlot;
    this->hullPosition = hullPosition;
    this->highPower = true;
    this->ship = ship;
    totalWorkspaces = NumberParser::getWorkspacesForSystem(ship->getClassSM());
    this->cost = NumberParser::getCost1(ship->getClassSM(), ship->getClassSM()+7);
}

ShipCloakingDeviceModule::~ShipCloakingDeviceModule()
{

}

/**
 * @brief ShipCloakingDeviceModule::text
 * @return returns parsed module stats for display
 * in a list widget.
 */
QString ShipCloakingDeviceModule::text() const
{
    QString returnValue = "";
    returnValue += "[" + QString::number(moduleSlot);
    if (highPower)
    {
        returnValue += "!]";
    }
    else
    {
    returnValue += "]";
    }
    returnValue += " Cloaking Device";
    returnValue += " Workspaces: " + QString::number(totalWorkspaces);
    returnValue += " Cost: " + NumberParser::formatHumanReadable(QString::number(cost));
    return returnValue;
}

/**
 * @brief ShipCloakingDeviceModule::setItemVisibleText
 * Sets the visible text when the item is in,
 * e.g., a QListWidget.
 */
void ShipCloakingDeviceModule::setItemVisibleText()
{
    setData(Qt::DisplayRole, this->text());
}

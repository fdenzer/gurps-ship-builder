#include "mainwindow.h"
#include "ui_mainwindow.h"

/**
 * @brief MainWindow::MainWindow
 * @param parent
 */
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->actionNew_Ship,SIGNAL(triggered()), this, SLOT(spawnNewShipWindow()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

/**
 * @brief MainWindow::spawnNewShipWindow
 */
void MainWindow::spawnNewShipWindow()
{
    shipOverviewWindow = new ShipOverview(this);
    shipOverviewWindow->show();
}

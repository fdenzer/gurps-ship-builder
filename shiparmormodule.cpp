#include "shiparmormodule.h"

/**
 * @brief ShipArmorModule::ShipArmorModule
 * @param hullPosition
 * @param moduleSlot
 */
ShipArmorModule::ShipArmorModule(int hullPosition, int moduleSlot)
{
    dDR = 0;
    armorType = "Ice";
    this->moduleSlot = moduleSlot;
    this->hullPosition = hullPosition;
}

ShipArmorModule::~ShipArmorModule()
{

}

/**
 * @brief ShipArmorModule::getdDR
 * @return
 */
int ShipArmorModule::getdDR()
{
    return dDR;
}

/**
 * @brief ShipArmorModule::setdDR
 * @param newdDR
 */
void ShipArmorModule::setdDR(int newdDR)
{
    dDR = newdDR;
}

/**
 * @brief ShipArmorModule::text
 * @return returns parsed module stats for display
 * in a list widget.
 */
QString ShipArmorModule::text() const
{
    QString returnValue = "";
    /*if (hullPosition == FRONT)
    {
        returnValue += "Front";
    }
    else if (hullPosition == CENTRAL)
    {
        returnValue += "Center";
    }
    else if (hullPosition == REAR)
    {
        returnValue += "Rear";
    }
    else if (hullPosition == CORE)
    {
        returnValue += "Core";
    }*/
    returnValue += "[" + QString::number(moduleSlot);
    if (highPower)
    {
        returnValue += "!]";
    }
    else
    {
    returnValue += "]";
    }
    returnValue += " " + armorType;
    returnValue += QString(" (dDR ") + QString::number(dDR) + QString(")");
    returnValue += " Cost: " + NumberParser::formatHumanReadable(QString::number(cost));
    //setData(Qt::DisplayRole, returnValue);
    return returnValue;
}

/**
 * @brief ShipArmorModule::getArmorType
 * @return
 */
QString ShipArmorModule::getArmorType()
{
    return armorType;
}

/**
 * @brief ShipArmorModule::setArmorType
 * @param type
 */
void ShipArmorModule::setArmorType(QString type)
{
    armorType = type;
}

/**
 * @brief ShipArmorModule::setItemVisibleText
 * Sets the visible text when the item is in,
 * e.g., a QListWidget.
 */
void ShipArmorModule::setItemVisibleText()
{
    setData(Qt::DisplayRole, this->text());
}

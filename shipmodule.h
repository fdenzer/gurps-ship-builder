#ifndef SHIPMODULE_H
#define SHIPMODULE_H
#include <QtGlobal>
#include <QString>
#include <QListWidgetItem>

class ShipModule : public QListWidgetItem
{
public:
    ShipModule();
    ShipModule(QString debugText, int hullPosition, int moduleSlot);
    virtual ~ShipModule();
    int getHullPosition();
    int getSlot();
    bool isHighPower();
    int getWorkspacesNeeded();
    int getCost();
    void setHullPosition(int position);
    void setSlot(int slot);
    void setHighPower(bool highPowah);
    void setWorkspaces(int workspaces);
    void setCost(int price);
    virtual QString text() const;
    virtual void setItemVisibleText();

protected:
    enum hullPositions { FRONT, CENTRAL, REAR, CORE };
    int hullPosition;
    int moduleSlot;
    bool highPower;
    int totalWorkspaces;
    qint64 cost;

private:
    QString debugText;
};

#endif // SHIPMODULE_H

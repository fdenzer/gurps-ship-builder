#-------------------------------------------------
#
# Project created by QtCreator 2015-02-02T21:02:24
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GURPSShipBuilder
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    shipoverview.cpp \
    spaceshipclass.cpp \
    armorwindow.cpp \
    numberparser.cpp \
    shipmodule.cpp \
    shiparmormodule.cpp \
    shipcloakingdevicemodule.cpp \
    shipdefensiveecmmodule.cpp \
    shipexternalclampmodule.cpp \
    shiprobotarmmodule.cpp \
    shipcontragravityliftermodule.cpp \
    habitatwindow.cpp \
    factorywindow.cpp

HEADERS  += mainwindow.h \
    shipoverview.h \
    spaceshipclass.h \
    armorwindow.h \
    numberparser.h \
    shipmodule.h \
    shiparmormodule.h \
    shipcloakingdevicemodule.h \
    shipdefensiveecmmodule.h \
    shipexternalclampmodule.h \
    shiprobotarmmodule.h \
    shipcontragravityliftermodule.h \
    habitatwindow.h \
    factorywindow.h

FORMS    += mainwindow.ui \
    shipoverview.ui \
    armorwindow.ui \
    habitatwindow.ui \
    factorywindow.ui

#include "spaceshipclass.h"

SpaceshipClass::SpaceshipClass()
{
    className = "";
    classSM = 5;
    classStreamlined = false;
}

SpaceshipClass::~SpaceshipClass()
{

}

QString SpaceshipClass::getClassName()
{
    return className;
}

void SpaceshipClass::setClassName(QString name)
{
    className = name;
}

int SpaceshipClass::getClassSM()
{
   return classSM;
}

void SpaceshipClass::setClassSM(int sm)
{
    classSM = sm;
}

int SpaceshipClass::getClassTonnage()
{
    int power = (classSM-2)/2;
    int tonnage = 0;
    if (classSM == (classSM/2)*2)
    {
        tonnage = 1*((int)(pow(10,power)+0.5));
    }
    else
    {
        tonnage = 3*pow(10,power);
    }
    return tonnage;
}

bool SpaceshipClass::getClassStreamlined()
{
    return classStreamlined;
}

void SpaceshipClass::setClassStreamlined(bool set)
{
    classStreamlined = set;
}

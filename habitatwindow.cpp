#include "habitatwindow.h"
#include "ui_habitatwindow.h"

HabitatWindow::HabitatWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::HabitatWindow)
{
    ui->setupUi(this);
}

HabitatWindow::~HabitatWindow()
{
    delete ui;
}

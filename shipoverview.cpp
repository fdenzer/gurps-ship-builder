#include "shipoverview.h"
#include "ui_shipoverview.h"

/**
 * @brief ShipOverview::ShipOverview
 * @param parent
 */
ShipOverview::ShipOverview(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::ShipOverview)
{
    ui->setupUi(this);

    // Ship object we're designing.
    ship = new SpaceshipClass();

    //Connect UI toolbar signals.
    connect(ui->actionExit,SIGNAL(triggered()), this, SLOT(exitWindow()));
    connect(ui->actionArmor,SIGNAL(triggered()), this, SLOT(armorWindow()));
    connect(ui->actionCloakingDevice,SIGNAL(triggered()),this,SLOT(addCloakingDevice()));
    connect(ui->actionDefensiveECM,SIGNAL(triggered()),this,SLOT(addDefensiveECM()));
    connect(ui->actionExternalClamp,SIGNAL(triggered()),this,SLOT(addExternalClamp()));
    connect(ui->actionRoboticArm, SIGNAL(triggered()), this, SLOT(addRoboticArm()));
    connect(ui->actionContragravityLifter, SIGNAL(triggered()), this, SLOT(addContragravityLifter()));

    ui->HullList->addItem("Front:");
    ui->HullList->addItem(new ShipModule("[1]", 0, 0));
    ui->HullList->addItem(new ShipModule("[2]", 0, 1));
    ui->HullList->addItem(new ShipModule("[3]", 0, 2));
    ui->HullList->addItem(new ShipModule("[4]", 0, 3));
    ui->HullList->addItem(new ShipModule("[5]", 0, 4));
    ui->HullList->addItem(new ShipModule("[6]", 0, 5));
    ui->HullList->addItem("Central:");
    ui->HullList->addItem(new ShipModule("[1]", 1, 0));
    ui->HullList->addItem(new ShipModule("[2]", 1, 1));
    ui->HullList->addItem(new ShipModule("[3]", 1, 2));
    ui->HullList->addItem(new ShipModule("[4]", 1, 3));
    ui->HullList->addItem(new ShipModule("[5]", 1, 4));
    ui->HullList->addItem(new ShipModule("[6]", 1, 5));
    ui->HullList->addItem("Rear:");
    ui->HullList->addItem(new ShipModule("[1]", 2, 0));
    ui->HullList->addItem(new ShipModule("[2]", 2, 1));
    ui->HullList->addItem(new ShipModule("[3]", 2, 2));
    ui->HullList->addItem(new ShipModule("[4]", 2, 3));
    ui->HullList->addItem(new ShipModule("[5]", 2, 4));
    ui->HullList->addItem(new ShipModule("[6]", 2, 5));
    ui->HullList->addItem("Core:");
    ui->HullList->addItem(new ShipModule("[1]", 3, 0));
    ui->HullList->addItem(new ShipModule("[2]", 3, 1));
}

ShipOverview::~ShipOverview()
{
    this->hide();
    delete ui;
}

/**
 * @brief ShipOverview::exitWindow
 * Black magic.
 */
void ShipOverview::exitWindow()
{
    this->hide();
    delete this;
}

/**
 * @brief ShipOverview::on_smBox_valueChanged
 * @param arg1
 * Called when the smBox value is changed by edit or tick.
 */
void ShipOverview::on_smBox_valueChanged(int arg1)
{
    ship->setClassSM(arg1);
    ui->smTonsBox->setText(NumberParser::addCommas(QString::number(ship->getClassTonnage())) + " Tons");
}

/**
 * @brief ShipOverview::on_classNameBox_editingFinished
 * When classNameBox loses focus or return is pressed.
 */
void ShipOverview::on_classNameBox_editingFinished()
{
    ship->setClassName(ui->classNameBox->text());
}

/**
 * @brief ShipOverview::on_checkBox_stateChanged
 * @param arg1
 */
void ShipOverview::on_checkBox_stateChanged(int arg1)
{
    ship->setClassStreamlined(arg1>0);
}

/**
 * @brief ShipOverview::armorWindow
 * Opens armor design window. Fix black magic cleanup.
 */
void ShipOverview::armorWindow()
{
    int hullPosition = ui->HullList->currentRow();
    int slotPosition = getSlotPositionInSection(hullPosition);
    if (slotPosition != 0)
    {
        ArmorWindow *armordiag = new ArmorWindow(this, ship, hullPosition, slotPosition);
        armordiag->show();
    }
}

void ShipOverview::addCloakingDevice()
{
    int hullPosition = ui->HullList->currentRow();
    int slotPosition = getSlotPositionInSection(hullPosition);
    if (slotPosition != 0)
    {
        ShipModule *cloak = new ShipCloakingDeviceModule(hullPosition,slotPosition,ship);
        addModule(cloak);
    }
}

void ShipOverview::addDefensiveECM()
{
    int hullPosition = ui->HullList->currentRow();
    int slotPosition = getSlotPositionInSection(hullPosition);
    if (slotPosition != 0)
    {
        ShipModule *ecm = new ShipDefensiveECMModule(hullPosition,slotPosition,ship);
        addModule(ecm);
    }
}

void ShipOverview::addExternalClamp()
{
    int hullPosition = ui->HullList->currentRow();
    int slotPosition = getSlotPositionInSection(hullPosition);
    if (slotPosition != 0)
    {
        ShipModule *clamp = new ShipExternalClampModule(hullPosition,slotPosition,ship);
        addModule(clamp);
    }
}

void ShipOverview::addRoboticArm()
{
    int hullPosition = ui->HullList->currentRow();
    int slotPosition = getSlotPositionInSection(hullPosition);
    if (slotPosition != 0)
    {
        ShipModule *arm = new ShipRobotArmModule(hullPosition,slotPosition,ship);
        addModule(arm);
    }
}

void ShipOverview::addContragravityLifter()
{
    int hullPosition = ui->HullList->currentRow();
    int slotPosition = getSlotPositionInSection(hullPosition);
    if (slotPosition != 0)
    {
        ShipModule *lifter = new ShipContragravityLifterModule(hullPosition,slotPosition,ship);
        addModule(lifter);
    }
}

/**
 * @brief ShipOverview::addModule
 * @param module
 * API call to pass modules back to this window from children.
 */
void ShipOverview::addModule(ShipModule *module)
{
    int currentRow = ui->HullList->currentRow();
    module->setItemVisibleText();
    delete ui->HullList->takeItem(currentRow);
    ui->HullList->insertItem(currentRow, module);
}

/**
 * @brief ShipOverview::getSlotPositionInSection
 * @param oldPosition
 * @return
 * Black magic and very unfinished.
 */
int ShipOverview::getSlotPositionInSection(int oldPosition)
{
    int slotPosition = oldPosition;
    if (oldPosition != 0 && oldPosition != 7 && oldPosition != 14 && oldPosition != 21)
    {
        if (slotPosition < 7)
        {
            slotPosition -= 0;
        }
        else if (slotPosition < 14)
        {
            slotPosition -= 7;
        }
        else if (slotPosition < 21)
        {
            slotPosition -= 14;
        }
        else
        {
            slotPosition -= 21;
        }
        return slotPosition;
    }
    return 0;
}

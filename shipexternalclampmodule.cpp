#include "shipexternalclampmodule.h"

ShipExternalClampModule::ShipExternalClampModule(int hullPosition, int moduleSlot, SpaceshipClass *ship)
{
    this->moduleSlot = moduleSlot;
    this->hullPosition = hullPosition;
    this->highPower = false;
    this->ship = ship;
    totalWorkspaces = 0;
    this->cost = NumberParser::getCost2(ship->getClassSM(), ship->getClassSM()+2);
}

ShipExternalClampModule::~ShipExternalClampModule()
{

}

QString ShipExternalClampModule::text() const
{
    QString returnValue = "";
    returnValue += "[" + QString::number(moduleSlot);
    if (highPower)
    {
        returnValue += "!]";
    }
    else
    {
    returnValue += "]";
    }
    returnValue += " External Clamp";
    returnValue += " Cost: " + NumberParser::formatHumanReadable(QString::number(cost));
    return returnValue;
}

/**
 * @brief ShipExternalClampModule::setItemVisibleText
 * Sets the visible text when the item is in,
 * e.g., a QListWidget.
 */
void ShipExternalClampModule::setItemVisibleText()
{
    setData(Qt::DisplayRole, this->text());
}

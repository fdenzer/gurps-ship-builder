#include "shiprobotarmmodule.h"

ShipRobotArmModule::ShipRobotArmModule(int hullPosition, int moduleSlot, SpaceshipClass *ship)
{
    this->moduleSlot = moduleSlot;
    this->hullPosition = hullPosition;
    this->highPower = false;
    this->ship = ship;
    totalWorkspaces = NumberParser::getWorkspacesForSystem(ship->getClassSM());
    this->cost = NumberParser::getCost2(ship->getClassSM(), ship->getClassSM()+6);
}

ShipRobotArmModule::~ShipRobotArmModule()
{

}

/**
 * @brief ShipRobotArmModule::text
 * @return returns parsed module stats for display
 * in a list widget.
 */
QString ShipRobotArmModule::text() const
{
    QString returnValue = "";
    returnValue += "[" + QString::number(moduleSlot);
    if (highPower)
    {
        returnValue += "!]";
    }
    else
    {
    returnValue += "]";
    }
    returnValue += " Robot Arm";
    returnValue += " Workspaces: " + QString::number(totalWorkspaces);
    returnValue += " Cost: " + NumberParser::formatHumanReadable(QString::number(cost));
    return returnValue;
}

/**
 * @brief ShipRobotArmModule::setItemVisibleText
 * Sets the visible text when the item is in,
 * e.g., a QListWidget.
 */
void ShipRobotArmModule::setItemVisibleText()
{
    setData(Qt::DisplayRole, this->text());
}

#ifndef SHIPEXTERNALCLAMPMODULE_H
#define SHIPEXTERNALCLAMPMODULE_H
#include "shipmodule.h"
#include "spaceshipclass.h"
#include "numberparser.h"

class ShipExternalClampModule : public ShipModule
{
public:
    ShipExternalClampModule(int hullPosition, int moduleSlot, SpaceshipClass *ship);
    ~ShipExternalClampModule();
    QString text() const;
    virtual void setItemVisibleText();

private:
    SpaceshipClass *ship;
};

#endif // SHIPEXTERNALCLAMPMODULE_H

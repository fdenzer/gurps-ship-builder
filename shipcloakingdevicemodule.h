#ifndef SHIPCLOAKINGDEVICEMODULE_H
#define SHIPCLOAKINGDEVICEMODULE_H
#include "shipmodule.h"
#include "spaceshipclass.h"
#include "numberparser.h"


class ShipCloakingDeviceModule : public ShipModule
{
public:
    ShipCloakingDeviceModule(int hullPosition, int moduleSlot, SpaceshipClass *ship);
    ~ShipCloakingDeviceModule();
    QString text() const;
    virtual void setItemVisibleText();

private:
    SpaceshipClass *ship;
};

#endif // SHIPCLOAKINGDEVICEMODULE_H
